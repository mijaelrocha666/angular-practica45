import { Component, OnInit } from '@angular/core';
import { curri } from '../curriculum-interface/curriculum.interface';

@Component({
  selector: 'app-curriculum',
  templateUrl: './curriculum.component.html',
  styleUrls: ['./curriculum.component.css']
})
export class CurriculumComponent implements OnInit {

  curriculum: curri = {
    nombre: '',
    direccion: '',
    aceptar: ''
  }

  constructor() { }

  ngOnInit(): void {
  }

  Ir(): void {
    console.log(this.curriculum.nombre);
    console.log(this.curriculum.direccion);
    console.log(this.curriculum.aceptar);  
  }

}
